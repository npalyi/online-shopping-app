class OrdersController < ApplicationController
    before_action :logged_in_user, only: [:create, :edit]

   def new
      @order = current_user.orders.new
   end


    def create
      @order = current_user.orders.new(params[:order]) 
      @order.totalprice = session[:cart_total]
      if @order.save
        flash[:success] = "Order created, thank you!"
        clear_cart
      redirect_to '/cart'
      else
        render '/cart'
      end
    end

  def eit
  end

  def show
    @user = current_user
    @orders = @user.orders.paginate(page: params[:page])
  end
  
  
  private

    def order_params
      totalprice = 100;
        
      params.require(:order).permit(100 => :totalprice)
    end
    
    
end
